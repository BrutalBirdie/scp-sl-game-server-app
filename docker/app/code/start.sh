#!/bin/bash

set -eu

echo "=> Create /app/data/"
mkdir -p /app/data/

echo "=> Create nginx dirs"
mkdir -p /run/nginx/log /run/nginx/lib

echo "=> Create /data/scp_conf/"
if [[ ! -d /app/data/scp_conf ]]; then
    cp -rv /app/code/scp_conf_default /app/data/scp_conf
fi

echo "=> Create SCPSL Server"
mkdir -p /run/steamcmd && \
cd /run/steamcmd/ && \
curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar zxvf - && \
./steamcmd.sh +login anonymous +force_install_dir /run/scpsl-server/ +app_update 996560 +quit

echo "=> Start Server"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon