#!/bin/bash

set -x

ID=$(jq -r ".id" CloudronManifest.json)
VERSION=$(jq -r ".version" CloudronManifest.json)

echo "=> Create Test Data dir"
mkdir -p ./cloudron_test/data ./cloudron_test/tmp ./cloudron_test/run

echo "=> Cleanup Test Data"
rm -rf ./cloudron_test/data/* ./cloudron_test/tmp/* ./cloudron_test/run/*

echo "=> Build test image"
docker build -t dr.cloudron.dev/$ID:$VERSION .

echo "=> Run `test` tag of build image"
docker run -ti --read-only \
    --volume $(pwd)/cloudron_test/data:/app/data:rw \
    --volume $(pwd)/cloudron_test/tmp:/tmp:rw \
    --volume $(pwd)/cloudron_test/run:/run:rw \
    dr.cloudron.dev/$ID:$VERSION \
    bash
